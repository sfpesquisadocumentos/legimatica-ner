import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import br.leg.senado.legimatica.googlenl.model.Entity;
import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.googlenl.model.Mention;
import br.leg.senado.legimatica.googlenl.model.Sentence;
import br.leg.senado.legimatica.googlenl.model.Token;
import br.leg.senado.legimatica.googlenl.parsers.GoogleCloudResultJsonParser;
import br.leg.senado.legimatica.sigen.model.Norma;
import br.leg.senado.legimatica.sigen.parsers.ListaNormasCsvParser;

public class EntidadeNomeadaTest {

	public static void main(String[] args) throws Exception {
		testaListaLeis();
	}
	
	public static void testaParseGoogle() throws IOException, URISyntaxException {
		URL res = Thread.currentThread().getContextClassLoader().getResource(
				//"txt-sentencas-json/2016/LEI-2016-13249/00006-art1_cpt.json");
				//"txt-sentencas-json/2016/LEI-2016-13283");
				"txt-sentencas-json/2013/LEI-2013-12852");
		File pasta = new File(res.toURI());
		if (pasta.isDirectory()) {
			for (File arq : pasta.listFiles()) {
				processaDispositivo(arq);
				print("---");
			}
		} else {
			processaDispositivo(pasta);
		}		
	}
	
	public static void testaListaLeis() throws MalformedURLException, IOException {
		try (InputStream listaLeisCsv = new URL("http://pesquisa-textual.senado.gov.br/lista_leis.csv").openStream()) {
			ListaNormasCsvParser parser = new ListaNormasCsvParser();
			List<Norma> normas = parser.parse(listaLeisCsv);
			print("Total: " + normas.size());
			print(normas.get(0));
		}
	}
	
	public static void processaDispositivo(File arq) throws IOException {
		
		try (InputStream jsonStream = new FileInputStream(arq)) { 
			GoogleTextAnalysis result = new GoogleCloudResultJsonParser().parse(jsonStream);
			
			for (Sentence sentence : result.getSentences()) {
				print(sentence.getText().getContent());
			}

			print("Entidades baseadas no POS Tagging: ");
			StringBuilder sb = new StringBuilder();
			boolean sequence = false;
			for (Token token : result.getTokens()) {
				if (token.getPartOfSpeech().getProper().equals("PROPER")) {
					if (sequence) sb.append(" ");
					sb.append(token.getText().getContent());
					sequence = true;
				} else {
					if (sequence) {
						print(" >> " + sb);
						sb = new StringBuilder();
					}
					sequence = false;
				}
			}
			if (sequence) {
				print(" >> " + sb);
			}

			print("Entidades reconhecidas pelo Google: ");
			for (Entity entity : result.getEntities()) {
				boolean isProper =  entity.getType().equals("PROPER");
				if (!isProper) {
					for (Mention mention : entity.getMentions()) {
						if (mention.getType().equals("PROPER")) {
							isProper = true;
							continue;
						}
					}
				}
				
//				if (isProper) {
					print(" >> " + entity.getName() + " (" + entity.getType() + ") ");
					for (Mention mention : entity.getMentions()) {
						print("    - " + mention.getText().getContent() + " (" + mention.getType() + ") ");
					}
//				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void print(Object message) {
		System.out.println(message);
	}
}
