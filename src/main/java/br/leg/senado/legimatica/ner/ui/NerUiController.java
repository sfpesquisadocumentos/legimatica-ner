package br.leg.senado.legimatica.ner.ui;

import java.util.Calendar;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.leg.senado.legimatica.AnaliseTextualService;
import br.leg.senado.legimatica.NormasService;
import br.leg.senado.legimatica.corpus.CorpusBrowser;
import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.ner.AnaliseEntidadesService;
import br.leg.senado.legimatica.ner.data.AnaliseNerNorma;
import br.leg.senado.legimatica.sigen.SigenService;
import br.leg.senado.legimatica.sigen.model.Norma;

@Controller
@RequestMapping("/ner/normas")
public class NerUiController {

	@Resource
	AnaliseEntidadesService analiseEntidadesService;

	@Resource
	NormasService normasService;
	
	@Resource
	SigenService sigenService;
	
	@Resource
	AnaliseTextualService analiseTextualService;

	@Resource
	CorpusBrowser corpus;
	
	@RequestMapping("")
	public String listaNormas(
			@RequestParam(value="ano", required=false) Integer ano,
			Model model) {
		
		if (ano == null) {
			ano = Calendar.getInstance().get(Calendar.YEAR);
		}
		Map<String, String> normasComEmentas = corpus.listaNormasComEmentas(ano);
		model.addAttribute("normasComEmentas", normasComEmentas);
		model.addAttribute("ano", ano);

		return "lista-normas-com-ementas";
	}

	@RequestMapping("/nome/{nomeNorma}")
	public String detalheNormaPorNome(@PathVariable String nomeNorma, Model model) {
		// Nome vem no formato TIPO-ANO-NUMERO
		String[] partes = nomeNorma.split("-");
		String nomeInvertido = String.format("%s-%s-%s", partes[0], Integer.valueOf(partes[2]), partes[1]); 
		Norma norma = normasService.getNormaPorNome(nomeInvertido);
		return detalheNorma(norma, model);
	}
	
	public String detalheNorma(Norma norma, Model model) {

		AnaliseNerNorma analise = analiseEntidadesService.analisaNorma(norma);
		GoogleTextAnalysis analiseTextual = analiseTextualService.getAnaliseCorpoNorma(norma.getTipo(), norma.getAno(), norma.getNumero());		
		String ementa = normasService.getEmenta(norma.getTipo(), norma.getAno(), norma.getNumero());
		//List<String> dispositivos = corpus.getTextoDispositivos(norma.getAno(), norma.getTipo(), norma.getNumero());
		
		model.addAttribute("norma", norma);
		model.addAttribute("ementa", ementa);
		model.addAttribute("analise", analise);
		model.addAttribute("sentencas", analiseTextual.getSentences());
		
		return "detalhe-norma";
	}
}
