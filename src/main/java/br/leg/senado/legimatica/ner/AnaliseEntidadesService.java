package br.leg.senado.legimatica.ner;

import java.io.File;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.leg.senado.legimatica.AnaliseTextualService;
import br.leg.senado.legimatica.NormasService;
import br.leg.senado.legimatica.TermosService;
import br.leg.senado.legimatica.googlenl.model.Entity;
import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.googlenl.model.Mention;
import br.leg.senado.legimatica.ner.data.AnaliseNerEntidade;
import br.leg.senado.legimatica.ner.data.AnaliseNerIndexador;
import br.leg.senado.legimatica.ner.data.AnaliseNerNorma;
import br.leg.senado.legimatica.sigen.SigenService;
import br.leg.senado.legimatica.sigen.model.InstanciaRef;
import br.leg.senado.legimatica.sigen.model.Norma;
import br.leg.senado.legimatica.sigen.model.Termo;
import br.leg.senado.legimatica.util.GenericJsonProcessor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AnaliseEntidadesService {

	@Autowired
	TermosService termosService;
	
	@Autowired
	NormasService normasService;
	
	@Autowired
	SigenService sigenService;
	
	@Autowired
	AnaliseTextualService analiseTextualService;

	@Value("${legimatica.vocabulario.basePath:/opt/normas/ner}")
	String vocabularioBasePath;

	@Value("${legimatica.analise-ner.basePath:/opt/normas/ner/analises}")
	String analisesBasePath;

	@Value("${legimatica.indexadores.basePath:/opt/normas/ner/indexadores}")
	String indexadoresBasePath;
	
	private Map<Long, Set<String>> nomesPorIdTermo = new HashMap<>();
	private Map<String, Set<Termo>> termosPorNome = new HashMap<>();
	
	@PostConstruct
	protected void initVocabulario() {
		// Cria dois maps com as expressões normalizadas de cada termo do vocabulário,
		// para conseguir fazer a busca de forma ágil em memória.
		// Faz persistência dos maps porque é muito demorado montá-los, indo ao SIGEN
		// várias vezes por termo.
		File baseFolder = new File(vocabularioBasePath);
		if (!baseFolder.exists() && !baseFolder.mkdirs()) {
			throw new AnaliseEntidadesException("Não foi possível criar a pasta " + vocabularioBasePath);
		}
		File fileTermosPorNome = new File(baseFolder, "termosPorNome.json");
		carregaVocabularioNormalizado(fileTermosPorNome);
	}

	@SuppressWarnings("unchecked")
	private void carregaVocabularioNormalizado(File fileTermosPorNome) {
		log.debug("Carregando vocabulários do disco...");
		try {
			ObjectMapper mapper = new GenericJsonProcessor().getObjectMapper();
			if (fileTermosPorNome.exists()) {
				this.termosPorNome = (Map<String, Set<Termo>>) mapper.readValue(fileTermosPorNome, termosPorNome.getClass());
			} else {
				montaVocabularioNormalizado();		
				mapper.writeValue(fileTermosPorNome, termosPorNome);
				log.debug("Arquivo {} gravado com {} termos.", fileTermosPorNome.getAbsolutePath(), termosPorNome.size());
			}
			
			for (Map.Entry<String, Set<Termo>> entry : termosPorNome.entrySet()) {
				for (Termo t : entry.getValue()) {
					Set<String> nomes = nomesPorIdTermo.get(t.getId());
					if (nomes != null) {
						nomes.add(entry.getKey());
					} else {
						nomes = new HashSet<>();
						nomes.add(entry.getKey());
						nomesPorIdTermo.put(t.getId(), nomes);
					}
				}
			}
			log.debug("Vocabulários carregados! {} termos, {} nomes", termosPorNome.size(), nomesPorIdTermo.size());
		} catch (Exception e) {
			throw new AnaliseEntidadesException(e);
		}
	}

	private void montaVocabularioNormalizado() {
		List<Termo> termos = termosService.getListaTermos();
		int cont = 1;
		log.debug("Montando vocabulário com termos normalizados...");
		for (Termo termo : termos) {
			if (termo != null && termo.getId() != null) {
				try {
					Set<String> expressoes = encontraNomeESiglaTermo(termo.getId());
					for (String expressao : expressoes) {
						if (termosPorNome.containsKey(expressao)) {
							termosPorNome.get(expressao).add(termo);
						} else {
							Set<Termo> setTermo = new HashSet<>();
							setTermo.add(termo);
							termosPorNome.put(expressao, setTermo);
						}
					}
					log.debug("Termo #{}: id = {}, expressoes = {}", cont++, termo.getId(), expressoes);
				} catch (Exception e) {
					log.error("Erro ao processar o termo " + termo.getId(), e);
				}
			}
		}
	}
	
	public void analisaNormas() {

		// Para cada norma
		List<Norma> normas = normasService.getNormas();

		File baseFolder = new File(analisesBasePath);
		if (!baseFolder.exists() && !baseFolder.mkdirs()) {
			throw new AnaliseEntidadesException("Não foi possível criar a pasta " + analisesBasePath);
		}

		GenericJsonProcessor json = new GenericJsonProcessor();
		for (Norma norma : normas) {
			try {
				
				log.info("Processando norma {}", norma.getNomePreferido());
				String analiseFilename = String.format("%s-%s-%s-ner.json", norma.getTipo(), norma.getAno(), norma.getNumero());
				File analiseFile = new File(baseFolder, analiseFilename);
				if (analiseFile.exists()) {
					log.info("  Análise da {} já existe em {}", norma.getNomePreferido(), analiseFile.getAbsolutePath());
				} else {				
					AnaliseNerNorma analiseNorma = analisaNorma(norma);
					
					// Salva análise da norma
					json.getObjectMapper().writeValue(analiseFile, analiseNorma);
					log.info("  {} processada e salva em {}", norma.getNomePreferido(), analiseFile.getAbsolutePath());
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}

		/*
		// Obtem todos os termos do SIGEN
		// Para cada norma:
			// Obtem os indexadores da norma no SIGEN
			
			// Obtem as entidades reconhecidas na ementa da norma.
			// Obtem as entidades reconhecidas no corpo da norma, ordenadas por saliência.
				// Agrupa as entidades iguais, somando sua saliência.
			// Identifica as entidades que são indexadores da norma.
			// Identifica as entidades que são termos do vocabulário do SIGEN.
		
			// Adicionar em uma base com as seguintes informações
				- Norma
					- id
					- ano
					- tamanho em sentenças
					- tamanho em tokens
				- Indexadores
					- termo
					- id Sigen
					- encontrado na ementa?
					- encontrado no corpo?
				- Entidades
					- termo
					- saliência
					- encontrado na ementa?
					- encontrado no corpo?
					- é indexador?
					- é termo do vocabulário?
					- tipo

		// Para a totalidade das normas
			//	Identifica as entidades que mais foram encontradas
			//  Identifica os indexadores mais encontrados
			//  Identifica a média de indexadores encontrados por norma (comparando ementa e corpo), por tipo, por ano ou década
			//  Identifica a quantidade de termos não indexadores encontrados (idem) 
		*/
		
		
	}

	public AnaliseNerNorma analisaNorma(Norma norma) {
		
		// Obtém os indexadores da norma
		Long idNorma = norma.getCodDocumento();
		Set<Termo> indexadores = getTermosIndexadores(idNorma);
		
		// Obtém a análise sintática e de entidades da norma (ementa e corpo)
		String tipoNorma = norma.getTipo();
		Integer anoNorma = norma.getAno();
		String numeroNorma = norma.getNumero();
		GoogleTextAnalysis analiseTextualEmenta = analiseTextualService.getAnaliseEmenta(tipoNorma, anoNorma, numeroNorma);
		GoogleTextAnalysis analiseTextualCorpo = analiseTextualService.getAnaliseCorpoNorma(tipoNorma, anoNorma, numeroNorma);

		// Analisa norma
		AnaliseNerNorma analiseNorma = new AnaliseNerNorma();
		analiseNorma.setId(idNorma);
		analiseNorma.setAno(anoNorma);
		analiseNorma.setDescricao(norma.getNomePreferido());
		analiseNorma.setTamanhoTokens(analiseTextualCorpo.getTokens().size());
		analiseNorma.setTamanhoSentencas(analiseTextualCorpo.getSentences().size());
		
		Set<Termo> indexadoresEmenta = new HashSet<>();
		Set<Termo> indexadoresCorpo = new HashSet<>();
		Set<Termo> indexadoresNaoEncontrados = new HashSet<>(indexadores);		
		
		// Para cada entidade da ementa, verifica se é indexador ou termo do vocabulário
		log.debug("Analisando entidades da ementa...");
		for (Entity entidade : analiseTextualEmenta.getEntities()) {
			log.debug("Analisando entidade: {} ({})", entidade.getName(), entidade.getType());
			AnaliseNerEntidade analiseEntidade = localizaEntidadeAnalisada(analiseNorma, entidade.getName(), entidade.getType());				
			if (analiseEntidade == null) {
				analiseEntidade = analisaEntidade(indexadores, entidade);				
				analiseEntidade.setTipoEntidade(entidade.getType());
				analiseEntidade.setTipoSubstantivo(entidade.getMentions().get(0).getType());
				analiseNorma.addEntidade(analiseEntidade);
			}
			analiseEntidade.setEncontradoNaEmenta(true);
			analiseEntidade.setSalienciaEmenta(analiseEntidade.getSalienciaEmenta() + entidade.getSalience());
			
			if (analiseEntidade.isIndexador()) {
				indexadoresEmenta.addAll(analiseEntidade.getTermosSigen());
				indexadoresNaoEncontrados.removeAll(analiseEntidade.getTermosSigen());
			}
		}
		
		// Para cada entidade do corpo da norma, verifica se é indexador ou termo do vocabulário			
		log.debug("Analisando entidades do corpo da norma...");
		for (Entity entidade : analiseTextualCorpo.getEntities()) {
			log.debug("Analisando entidade: {} ({})", entidade.getName(), entidade.getType());
			AnaliseNerEntidade analiseEntidade = localizaEntidadeAnalisada(analiseNorma, entidade.getName(), entidade.getType());				
			if (analiseEntidade == null) {
				analiseEntidade = analisaEntidade(indexadores, entidade);
				analiseEntidade.setTipoEntidade(entidade.getType());
				analiseEntidade.setTipoSubstantivo(entidade.getMentions().get(0).getType());
				analiseNorma.addEntidade(analiseEntidade);
			}
			analiseEntidade.setEncontradoNoCorpo(true);
			analiseEntidade.setSalienciaCorpo(analiseEntidade.getSalienciaCorpo() + entidade.getSalience());
			
			if (analiseEntidade.isIndexador()) {
				indexadoresCorpo.addAll(analiseEntidade.getTermosSigen());
				indexadoresNaoEncontrados.removeAll(analiseEntidade.getTermosSigen());
			}
		}
		
		// Reune o resultado por indexador
		for (Termo indexador : indexadores) {
			AnaliseNerIndexador analiseIndexador = new AnaliseNerIndexador();
			Set<String> nomes = nomesPorIdTermo.get(indexador.getId());
			if (nomes == null) {
				log.warn("Muito estranho! Não encontrados nomes para termo {}, id {}.", indexador.getTexto(), indexador.getId());
				nomes = Collections.singleton(indexador.getTexto());
			}
			analiseIndexador.setNome(String.join(", ", nomes.stream().sorted().collect(Collectors.toList())));
			analiseIndexador.setTermoSigen(indexador);
			analiseIndexador.setEncontradoNaEmenta(indexadoresEmenta.contains(indexador));
			analiseIndexador.setEncontradoNoCorpo(indexadoresCorpo.contains(indexador));
			analiseNorma.addIndexador(analiseIndexador);
		}
		
		return analiseNorma;
	}

	private AnaliseNerEntidade localizaEntidadeAnalisada(AnaliseNerNorma analiseNorma, String name, String tipo) {
		for (AnaliseNerEntidade entidadeAnalisada : analiseNorma.getEntidades()) {
			if (entidadeAnalisada.getId().equals(name) && entidadeAnalisada.getTipoEntidade().equals(tipo)) {
				return entidadeAnalisada;
			}
		}
		return null;
	}

	private AnaliseNerEntidade analisaEntidade(Set<Termo> indexadoresNorma, Entity entidade) {
		AnaliseNerEntidade analiseEntidade = new AnaliseNerEntidade();
		analiseEntidade.setId(entidade.getName());
		Set<String> entityTexts = new HashSet<>();
		for (Mention mention : entidade.getMentions()) {
			entityTexts.add(mention.getText().getContent());
		}
		analiseEntidade.setTextos(StringUtils.join(entityTexts, ", "));

		Set<String> entidadeNormalizada = normalizaEntidade(entidade);		
		
		Set<Termo> termosEncontrados = new HashSet<>();
		for (String expressao : entidadeNormalizada) {
			Set<Termo> termosDaExpressao = termosPorNome.get(expressao);
			if (termosDaExpressao != null) {
				termosEncontrados.addAll(termosDaExpressao);
			}
		}
		if (!termosEncontrados.isEmpty()) {
			analiseEntidade.setVocabulario(true);
			analiseEntidade.setTermosSigen(termosEncontrados);
			if (!Collections.disjoint(indexadoresNorma, termosEncontrados)) {
				analiseEntidade.setIndexador(true);
			}
		}
		return analiseEntidade;
	}
	
	private Set<String> normalizaEntidade(Entity entidade) {
		Set<String> entidadeNormalizada = new HashSet<>(normaliza(entidade.getName()));
		for (Mention mention : entidade.getMentions()) {
			entidadeNormalizada.addAll(normaliza(mention.getText().getContent()));
			// TODO Tem como considerar o lemma?
		}
		log.debug("Entidade: {} ({}) - {}", entidade.getName(), entidade.getType(), entidadeNormalizada);
		return entidadeNormalizada;
	}

	protected Set<Termo> getTermosIndexadores(Long idNorma) {
		log.debug("  Identificando os termos indexadores da norma...");
		File baseFolder = new File(indexadoresBasePath);
		if (!baseFolder.exists() && !baseFolder.mkdirs()) {
			throw new AnaliseEntidadesException("Não foi possível criar a pasta " + indexadoresBasePath);
		}
		
		GenericJsonProcessor json = new GenericJsonProcessor();
		String filename = String.format("%010d-indexadores.json", idNorma);
		File indexadoresFile = new File(baseFolder, filename);
		if (indexadoresFile.exists()) {
			// Carrega a lista de indexadores desta norma
			log.debug("    Carregando de {}", indexadoresFile.getAbsolutePath());
			try {
				return json.getObjectMapper().readValue(indexadoresFile, 
						json.getObjectMapper().getTypeFactory()
						.constructCollectionType(HashSet.class, Termo.class));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				indexadoresFile.renameTo(new File(baseFolder, filename + ".erro"));
			}
		}
		
		Set<Termo> indexadores = new HashSet<>();
		// Busca indexação do SIGEN
		List<InstanciaRef> linksIndexacao = sigenService.getPropriedadeInstanciaRef(idNorma, "relacaoDocumentoIndexacao"); 
		if (linksIndexacao != null) {
			// Para cada frase de Indexação
			for (InstanciaRef link : linksIndexacao) {
				List<InstanciaRef> linksFraseParticular = sigenService.getPropriedadeInstanciaRef(
						Long.valueOf(link.getName()), "relacaoFraseIndexacaoParticular");
				
				// Obtém cada frase de indexação particular
				for (InstanciaRef linkParticular : linksFraseParticular) {
					indexadores.addAll(encontraTermosIndexadores(linkParticular));
				}
			}
		}
		
		// Salva a lista de indexadores desta norma
		try {
			log.debug("    Salvando em {}", indexadoresFile.getAbsolutePath());
			json.getObjectMapper().writeValue(indexadoresFile, indexadores);
		} catch (Exception e) {
			throw new AnaliseEntidadesException("Não foi possível criar o arquivo " + indexadoresFile.getAbsolutePath());
		}
		return indexadores;
	}

	private Set<Termo> encontraTermosIndexadores(InstanciaRef instanciaRef) {
		Set<Termo> termos = new HashSet<>();
		Termo termo = termosService.localizaTermoPorId(Long.valueOf(instanciaRef.getName()));
		if (termo == null) {
			List<InstanciaRef> termosPreferenciais = sigenService.getPropriedadeInstanciaRef(
					Long.valueOf(instanciaRef.getName()), "denotadoPreferencialmentePor");
			for (InstanciaRef ref : termosPreferenciais) {
				termos.addAll(encontraTermosIndexadores(ref));
			}
			List<InstanciaRef> termosAlternativos = sigenService.getPropriedadeInstanciaRef(
					Long.valueOf(instanciaRef.getName()), "denotadoAlternativamentePor"); 
			if (termosAlternativos != null) {
				for (InstanciaRef refAlt : termosAlternativos) {
					termos.addAll(encontraTermosIndexadores(refAlt));
				}
			}
		} else {
			log.debug("  Indexador identificado: {}, id {}, tipo {}", termo.getTexto(), termo.getId(), termo.getP());
			termos.add(termo);
		}
		return termos;
	}	
	
	protected Set<String> encontraNomeESiglaTermo(Long idInstancia) {
		Set<String> expressoes = new HashSet<>();
		String nomeTermo = sigenService.getPropriedade(idInstancia, "nomeDeTermo", String.class);
		if (nomeTermo != null) {
			expressoes.addAll(normaliza(nomeTermo));
			String siglaTermo = sigenService.getPropriedade(idInstancia, "siglaDeTermo", String.class);
			if (siglaTermo != null) {
				expressoes.addAll(normaliza(siglaTermo.replaceAll("[\\(\\)]", ""))); // Remove os parênteses da sigla 
			}
			log.debug("    Expressão {}, sigla {}, id {}", nomeTermo, siglaTermo, idInstancia);
		} else {
			List<InstanciaRef> termosPreferenciais =
					sigenService.getPropriedadeInstanciaRef(idInstancia, "denotadoPreferencialmentePor");
			if (termosPreferenciais != null) {
				for (InstanciaRef ref : termosPreferenciais) {
					Long idRef = Long.valueOf(ref.getName());
					expressoes.addAll(encontraNomeESiglaTermo(idRef));
				}
			}
			List<InstanciaRef> termosAlternativos =
					sigenService.getPropriedadeInstanciaRef(idInstancia, "denotadoAlternativamentePor");
			if (termosAlternativos != null) {
				for (InstanciaRef ref : termosAlternativos) {
					Long idRef = Long.valueOf(ref.getName());
					expressoes.addAll(encontraNomeESiglaTermo(idRef));
				}
			}
		}
		return expressoes;
	}
	
	
	private static final Pattern PATTERN_SIGLA_PARENTESES = Pattern.compile("\\(([^\\)]*)\\)");
	
	protected Set<String> normaliza(String termo) {
		if (termo == null) {
			return Collections.emptySet();
		}
		Set<String> termos = new HashSet<>();
		
		// Remove acentos e espaços em excesso e coloca em caixa alta
		// (ex.: " Presidência " -> "PRESIDENCIA")
		String semAcentos = Normalizer.normalize(termo, Normalizer.Form.NFD)
			.replaceAll("\\p{InCombiningDiacriticalMarks}+", "") // Remove acentos
			.replaceAll("(\\S)\\-(\\S)", "$1 $2") // Remove hífens de palavras compostas
			.replaceAll("\\s{2,}", " ") // Remove espaços múltiplos
			.trim()
			.toUpperCase();
		termos.add(semAcentos); // Termo completo sem acentos

		String termoPrincipal = semAcentos;
		
		// Separa sigla entre parênteses
		// (ex.: "Imposto de Renda Pessoa Física (IRPF)" -> "Imposto de Renda Pessoa Física", "IRPF")
		Matcher matcher = PATTERN_SIGLA_PARENTESES.matcher(termoPrincipal);
		boolean temSigla = false;
		while (matcher.find()) {
			String sigla = matcher.group(1);
			termos.add(sigla); // Sigla
			temSigla = true;
		}
		if (temSigla) {
			termoPrincipal = semAcentos.replaceAll(PATTERN_SIGLA_PARENTESES.pattern(), "").trim();
		}
		
		// Extrai expressões separadas por hífen
		// (ex.: "Imposto de Renda Pessoa Física - IRPF" -> "Imposto de Renda Pessoa Física", "IRPF")
		if (termoPrincipal.contains(" - ")) {
			termos.addAll(Arrays.asList(termoPrincipal.split(" - ")));
		}
		
		// Adiciona o termo principal completo
		termos.add(termoPrincipal);
		
		return termos;
	}
	
/*	private static List<String> STOP_WORDS = Arrays.asList(
			"E", "A", "O", "AS", "OS", "AO", "AOS", "DE", "DA", "DAS", "DO", "DOS", "EM", "NA", "NO", "NAS", "NOS", "COM", "POR", "PARA");

	private static String[] DE = { "DA", "DO", "DOS", "DAS" };
	private static String[] EM = { "NA", "NO", "NAS", "NOS" };
	private static String[] POR = { "PELO", "PELA", "PELOS", "PELAS" };
	
	private String removeStopWords(String termo) {
		for (String stopWord : STOP_WORDS) {
			termo = termo.replaceAll("\\b"+stopWord+"\\b", "");
		}
		termo = termo.replaceAll("\\s{2,}", " "); // Remove espaços múltiplos
		return termo.trim();
	}
*/	
	@SuppressWarnings("serial")
	public static class AnaliseEntidadesException extends RuntimeException {
		public AnaliseEntidadesException(String message) {
			super(message);
		}
		public AnaliseEntidadesException(Throwable t) {
			super(t.getMessage(), t);
		}
	}
}
