package br.leg.senado.legimatica.ner;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="br.leg.senado.legimatica")
@EnableCaching
public class LegimaticaNerApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	AnaliseEntidadesService analiseEntidadesService;
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(LegimaticaNerApplication.class).web(true).run(args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LegimaticaNerApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		if (Arrays.asList(args).contains("-analise")) {
			analiseEntidadesService.analisaNormas();
		}
	}
}
