package br.leg.senado.legimatica.ner.ui;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.leg.senado.legimatica.AnaliseTextualService;
import br.leg.senado.legimatica.NormasService;
import br.leg.senado.legimatica.corpus.CorpusBrowser;
import br.leg.senado.legimatica.googlenl.model.Conll;
import br.leg.senado.legimatica.googlenl.model.GoogleTextAnalysis;
import br.leg.senado.legimatica.sigen.SigenService;
import br.leg.senado.legimatica.sigen.model.InstanciaCompacta;
import br.leg.senado.legimatica.sigen.model.Norma;

@Controller
@RequestMapping("/syntax/normas")
public class SyntaxUiController {

	@Resource
	NormasService normasService;

	@Resource
	SigenService sigenService;
	
	@Resource
	AnaliseTextualService analiseTextualService;

	@Resource
	CorpusBrowser corpus;
	
	@RequestMapping("")
	public String listaNormas(
			@RequestParam(value="ano", required=false) Integer ano,
			Model model) {
		
		if (ano == null) {
			ano = Calendar.getInstance().get(Calendar.YEAR);
		}
		Map<String, String> normasComEmentas = corpus.listaNormasComEmentas(ano);
		model.addAttribute("normasComEmentas", normasComEmentas);
		model.addAttribute("ano", ano);

		return "lista-normas-com-ementas";
	}

	@RequestMapping("/nome/{nomeNorma}")
	public String detalheNormaPorNome(@PathVariable String nomeNorma, Model model) {
		// Nome vem no formato TIPO-ANO-NUMERO
		String[] partes = nomeNorma.split("-");
		String nomeInvertido = String.format("%s-%s-%s", partes[0], partes[2], partes[1]); 
		Norma norma = normasService.getNormaPorNome(nomeInvertido);
		return detalheNorma(norma, model);
	}
	
	public String detalheNorma(Norma norma, Model model) {
		
		// Busca detalhes do SIGEN
//		InstanciaCompacta detalheInstancia = sigenService.getDetalheInstancia(norma.getCodDocumento());
		model.addAttribute("descricao", norma.getNomePreferido());

		String ementa = normasService.getEmenta(norma.getTipo(), norma.getAno(), norma.getNumero());
		model.addAttribute("ementa", ementa);
				
		// Busca análise das sentenças		
		Map<String, GoogleTextAnalysis> analise = analiseTextualService.getAnaliseNorma(norma.getTipo(), norma.getAno(), norma.getNumero());
		model.addAttribute("analiseTextual", analise);
		
		Map<String, List<String>> conll = new HashMap<>();
		for (Entry<String, GoogleTextAnalysis> entry : analise.entrySet()) {
			conll.put(entry.getKey(), Conll.toConllX(entry.getValue()));
		}
		model.addAttribute("conll", conll);
				
		return "syntax-norma";
	}
}
