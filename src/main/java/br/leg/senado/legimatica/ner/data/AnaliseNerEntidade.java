package br.leg.senado.legimatica.ner.data;

import java.util.Set;

import br.leg.senado.legimatica.sigen.model.Termo;
import lombok.Data;

@Data
public class AnaliseNerEntidade {

	String id;
	String textos;
	double salienciaEmenta = Double.valueOf(0);
	double salienciaCorpo = Double.valueOf(0);
	boolean encontradoNaEmenta = false;
	boolean encontradoNoCorpo = false;
	boolean indexador = false;
	boolean vocabulario = false;
	Set<Termo> termosSigen;
	String tipoEntidade;
	String tipoSubstantivo;
}
