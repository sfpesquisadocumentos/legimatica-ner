package br.leg.senado.legimatica.ner.data;

import br.leg.senado.legimatica.sigen.model.Termo;
import lombok.Data;

@Data
public class AnaliseNerIndexador {
	String nome;
	Termo termoSigen;
	boolean encontradoNaEmenta;
	boolean encontradoNoCorpo;	
}
