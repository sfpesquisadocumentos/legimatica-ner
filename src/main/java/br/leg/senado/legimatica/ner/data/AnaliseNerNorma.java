package br.leg.senado.legimatica.ner.data;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AnaliseNerNorma {

	Long id;
	String descricao;
	Integer ano;
	Integer tamanhoSentencas;
	Integer tamanhoTokens;
	
	List<AnaliseNerIndexador> indexadores = new ArrayList<>();
	List<AnaliseNerEntidade> entidades = new ArrayList<>();
	
	public void addEntidade(AnaliseNerEntidade analiseEntidade) {
		entidades.add(analiseEntidade);
	}
	
	public void addIndexador(AnaliseNerIndexador analiseIndexador) {
		indexadores.add(analiseIndexador);
	}
}
